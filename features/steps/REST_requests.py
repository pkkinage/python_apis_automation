import requests
from requests.auth import HTTPDigestAuth

# -------------------------------------------------------------------------------------------
# This python file contains the methods to make different types of API requests (get,put,post)
# -------------------------------------------------------------------------------------------

# make a POST request based on the parameters received
# url parameter expects the api endpoint on which the request needs to be made
# headers parameter expects the headers that need to be sent with the request
# payload parameter expects the payload in xml format that needs to be sent to an api endpoint
# auth parameter expects the authentication headers for the request
# proxies parameter expects the proxy information in order for the request to go through
def post_request(context, url, headers, payload, auth, proxies):
    print("POST Request URL: %s" % url)
    print("Request headers: %s" % headers)
    print("Request payload: %s" % payload)
    if auth is not None:
        print("Request Auth: %s" % auth)

    # get the response in a variable
    response = requests.post(url=url, headers=headers, data=payload, auth=auth, proxies=proxies)

    print("Response StatusCode: %s" % response.status_code)
    print("Response headers: %s" % response.headers)
    print("Response body: %s" % response.text)

    # store the response in a dictionary
    response = {
        "status_code": response.status_code,
        "headers": response.headers,
        "body": response.text
    }
    context.response = response

# make a PUT request based on the parameters received
# url parameter expects the api endpoint on which the request needs to be made
# headers parameter expects the headers that need to be sent with the request
# payload parameter expects the payload in xml format that needs to be sent to an api endpoint
# auth parameter expects the authentication headers for the request
# proxies parameter expects the proxy information in order for the request to go through
def put_request(context, url, headers, payload, auth, proxies):
    print("PUT Request URL: %s" % url)
    print("Request headers: %s" % headers)
    print("Request payload: %s" % payload)
    if auth is not None:
        print("Request Auth: %s" % auth)

    # get the response in a variable
    response = requests.put(url=url, headers=headers, data=payload, auth=auth, proxies=proxies)

    print("Response StatusCode: %s" % response.status_code)
    print("Response headers: %s" % response.headers)
    print("Response body: %s" % response.text)

    # store the response in a dictionary
    response = {
        "status_code": response.status_code,
        "headers": response.headers,
        "body": response.text
    }
    context.response = response

# make a GET request based on the parameters received
# url parameter expects the api endpoint on which the request needs to be made
# headers parameter expects the headers that need to be sent with the request
# payload parameter expects the payload in xml format that needs to be sent to an api endpoint
# auth parameter expects the authentication headers for the request
# proxies parameter expects the proxy information in order for the request to go through
def get_request(context, url, headers, auth, proxies):
    print("GET Request URL: %s" % url)
    print("Request headers: %s" % headers)
    if auth is not None:
        print("Request Auth: %s" % auth)

    response = requests.get(url,headers=headers,auth=auth,proxies=proxies)

    print("Response StatusCode: %s" % response.status_code)
    print("Response headers: %s" % response.headers)
    print("Response body: %s" % response.text)

    response = {
        "status_code": response.status_code,
        "headers": response.headers,
        "body": response.text
    }
    context.response = response

