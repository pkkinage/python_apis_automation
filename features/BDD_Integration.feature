#Archive: Time Duration between start date And end date is more Then one month.
#catchup: Time Duration between start date And end date is less Then one month.
Feature: Category - First Time Publish

	@wip
	Scenario: Verify that catchup content publication is successful.
	Given Valid Catchup schedule entry in Movida 
	And   Media Rendition is complete with Technical Metadata.
	When  Published from Movida.
	Then  Packager should generate version 1 tar file should be generated.   
	And   RSS feed is successfully updated. 
	And   Success Notification is received in Movida. 
