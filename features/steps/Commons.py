# from steps.Content import *
import xml.etree.ElementTree as ET
from features.steps.Content import *
from features.steps.REST_requests import post_request, put_request, get_request
from features.steps.helpers import xlRead, valid_title_attributes, get_Title_payload, get_API_endpoints, \
    get_auth_headers, check_status_code, get_start_time, get_end_time, get_external_id, platform_urls, get_platform, \
    get_schedule_payload, get_Images_payload, get_Asset_payload, get_first_shown_on_linear, get_Title_Metadata_payload, \
    get_Asset_Metadata_payload, get_workflow_payload, read_json

http_request_headers = {}
global_general_variables = {}
http_request_headers['Content-Type'] = 'application/xml'
proxies = {
    'http': 'proxy.intra.bt.com:80',
    'https': 'proxy.intra.bt.com:80',
}

# ------------------------------------------------------------
# All the methods defined here follow the same basic structure
# ------------------------------------------------------------

# function to create a title with minimal fields required
def create_Title(context):
    # read required values from the excel sheet (title name,etc..)
    xlRead(context)
    # gather all the data needed to make a request
    title_attributes = valid_title_attributes(context)
    licensor_url= get_licensor_URL(context)
    xml = get_Title_payload(title_attributes['Title_Name'] ,licensor_url, title_attributes['Title_Type'], title_attributes['Title_Tag'],title_attributes['external_id'])
    endpoints = get_API_endpoints(context)
    url = endpoints['Title']
    auth = get_auth_headers(context)
    # make a request once all the data to be sent with the request is available
    post_request(context,url,http_request_headers,xml,auth,proxies)
    global_general_variables['title_self'] = get_related_API_url(context,'self')
    # verify the status of the request made
    check_status_code(context, 201)

# function to create a schedule for the title
def create_Schedule(context):
    title_attributes = valid_title_attributes(context)
    # get the start time and the end time of the schedule
    if not title_attributes['put-up-time']:
        put_up_time = get_start_time()
    else :
        put_up_time = title_attributes['put-up-time']
    if not title_attributes['put-up-time']:
        take_down_time = get_end_time()
    else :
        take_down_time = title_attributes['take-down-time']
    # gather all the data needed to make a request
    external_id = get_external_id(context)
    platform_url = platform_urls[get_platform(context)]

    xml = get_schedule_payload(put_up_time,take_down_time,external_id,platform_url)
    url = get_scheduling_url(context)
    auth = get_auth_headers(context)
    # make a request once all the data to be sent with the request is available
    post_request(context, url, http_request_headers, xml, auth, proxies)
    # verify the status of the request made
    check_status_code(context, 201)

# send the title to the sequence API for further processing
def send_to_Sequence(context):
    # gather all the data needed to make a request
    url = get_related_API_url(context,'workflow')
    xml = get_workflow_payload()
    auth = get_auth_headers(context
    # make a request once all the data to be sent with the request is available
    post_request(context, url, http_request_headers, xml, auth, proxies)
    # verify the status of the request made
    check_status_code(context, 200)

# function to add the images for the title.
def add_Images(context):
    # gather all the data needed to make a request
    rel_urls = get_title_related_APIs(context)
    url = rel_urls['images']
    image_file_urls=['https://media.gettyimages.com/photos/sergio-kun-aguero-of-manchester-city-in-action-during-the-uefa-of-picture-id1132309248',
                     'https://cdn.pixabay.com/photo/2014/10/14/20/24/the-ball-488700_960_720.jpg',
                     'https://media.gettyimages.com/photos/raheem-sterling-of-manchester-city-scores-the-fifth-goal-to-make-it-picture-id1126287032',
                     'https://media.gettyimages.com/photos/spanish-referee-carlos-del-cerro-grande-shows-the-red-card-to-citys-picture-id1126281038',
                     'https://media.gettyimages.com/photos/schalkes-algerian-midfielder-nabil-bentaleb-shoots-a-penalty-kick-to-picture-id1126280076'
                     ]
    # keep adding the images in a loop till all the images are uploaded
    for i in image_file_urls:
        image_file_url = i
        xml = get_Images_payload(image_file_url)
        auth = get_auth_headers(context)
        # make a request with the appropriate payload to the images API
        post_request(context, url, http_request_headers, xml, auth, proxies)
        # verify the status of the request made
        check_status_code(context, 201)

# function to create an asset
def create_Asset(context):
    # gather all the data needed to make a request
    rel_urls = get_title_related_APIs(context)
    url = rel_urls['assets']
    asset_name = 'BBJ'+get_external_id(context)
    description = 'Sample Desc'
    xml = get_Asset_payload(asset_name,description)
    auth  = get_auth_headers(context)
    # make a request once all the data to be sent with the request is available
    post_request(context, url, http_request_headers, xml, auth, proxies)
    # verify the status of the request made
    check_status_code(context, 201)

# function to update the title metadata
# this function to be called only after the title has been ingested
def update_title_metadata(context):
    # gather all the data needed to make a request
    title_attributes = valid_title_attributes(context)
    url = get_related_API_url(context,'metadata')
    first_shown_on_linear = get_first_shown_on_linear(context)
    xml = get_Title_Metadata_payload(title_attributes['metadata_type'],title_attributes['Title_Name'],title_attributes['short_description'],title_attributes['medium_description'],title_attributes['long_description'],title_attributes['category'],title_attributes['subgenre'],title_attributes['release_year'],title_attributes['production_company_location'],first_shown_on_linear,title_attributes['linear_channel'],title_attributes['sky_placements'])
    auth = get_auth_headers(context)
    # make a request once all the data to be sent with the request is available
    put_request(context, url, http_request_headers, xml, auth, proxies)
    # verify the status of the request made
    check_status_code(context, 200)

# function to update the asset metadata
# this function to be called only after the asset has been created
def update_asset_metadata(context):
    # gather all the data needed to make a request
    title_attributes = valid_title_attributes(context)
    url = get_related_API_url(context,'metadata')
    xml = get_Asset_Metadata_payload(title_attributes['rating'])
    auth = get_auth_headers(context)
    # make a request once all the data to be sent with the request is available
    put_request(context, url, http_request_headers, xml, auth, proxies)
    # verify the status of the request made
    check_status_code(context, 200)

# function to get the the licensor url
# the licensor url is needed while creating the title
def get_licensor_URL(context):
    # gather all the data needed to make a request
    auth = get_auth_headers(context)
    endpoints = get_API_endpoints(context)
    license_url_request = endpoints['licensor']
    # make a request once all the data to be sent with the request is available
    get_request(context, license_url_request, http_request_headers,auth,proxies)
    check_status_code(context , 200)
    print('Retrieving licensor')
    licensor_href = get_related_API_url(context,'self')

    return licensor_href

# whenever a request is made to any of the APIs, all the related APIs are returned as a response body
# this method returns the API endpoint of one of the related APIs.
# pass as a parameter the api for which the endpoint url is required
def get_related_API_url(context,rel_name):

    # store the response body in a variable
    related_xml = context.response['body']
    # use the ElementTree library to parse the xml response
    root = ET.fromstring(related_xml)

    for nodes in root.iter('link'):
        rel = nodes.get('rel')
        href = nodes.get('href')
        if rel == rel_name:
            related_endpoint = href
            break

    return related_endpoint

# function to get the scheduling url for each title
def get_scheduling_url(context) :
    # gather all the data needed to make a request
    auth = get_auth_headers(context)
    rel_urls = get_title_related_APIs(context)
    url = rel_urls['schedule']
    # make a request once all the data to be sent with the request is available
    get_request(context, url, http_request_headers, auth, proxies)
    return get_related_API_url(context,'schedulings')

# function to get all the related API urls of the title and store it in a dictionary.
def get_title_related_APIs(context) :

    auth = get_auth_headers(context)
    get_request(context,global_general_variables['title_self'],http_request_headers,auth,proxies)
    related_api_urls = {
      "self" : get_related_API_url(context,'self'),
      "schedule" : get_related_API_url(context,'schedule'),
      "availability_windows" : get_related_API_url(context,'availability_windows'),
      "title_groups" : get_related_API_url(context,'title_groups'),
      "licensor" : get_related_API_url(context,'licensor'),
      "images" : get_related_API_url(context,'images'),
      "assets" : get_related_API_url(context,'assets'),
      "metadata" : get_related_API_url(context,'metadata'),
      "blackouts" : get_related_API_url(context,'blackouts'),
      "rights" : get_related_API_url(context,'rights'),
      "trailers" : get_related_API_url(context,'trailers'),
    }
    return related_api_urls



