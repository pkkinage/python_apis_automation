# Configuration information over here
#Reporting to be done here

from allure import *
from allure.allure import Report
# from allure_behave import *
# from allure_behave.formatter2 import AllureBehaveListener
from allure_commons import *

# ----------------------------------------------
# This file contains the reporting functionality
# Use the allure reporting to generate reports
# ----------------------------------------------


def before_all(context):
    report_dir_name = "report"
    browser_name = "None"
    base_url = "https://preproduction-movida.bebanjo.net/api"

    context.allure  = Report(report_dir_name,"VOD TO SKY Tests",browser_name,base_url,re_create=False)

def before_feature(context , feature):
    context.allure.before_feature(feature)

def before_scenario(context , feature):
    context.allure.before_scenario(feature)

def before_step(context, step):
    context.allure.before_step(step)

def after_scenario(context, scenario):
    context.allure.after_scenario(scenario)

def after_step(context, step):
    context.allure.after_step(step,attachments=None)

def after_all(context):
    context.allure.after_all()

