from behave import *
import requests
import json
from requests.auth import HTTPDigestAuth

from features.steps import Commons

global_general_variables = {}
http_request_headers = {}
proxies = {
    'http': 'proxy.intra.bt.com:80',
    'https': 'proxy.intra.bt.com:80',
}

# ------------------------------------------------------------------------------
# The purpose of this Python file is to give an abstract view of the application
# ------------------------------------------------------------------------------

# set the base url of the application
def set_base_url(context, url):
    context.basic_application_url = url

# function to call all the title related methods
def create_update_title(context):
    Commons.create_Title(context)
    Commons.update_title_metadata(context)

# set the headers
def set_headers(context):
    http_request_headers['Content-Type'] = 'application/xml'

# function to call all the asset related methods
def add_asset_metadata(context):
    Commons.create_Asset(context)
    Commons.update_asset_metadata(context)

# function to call the add image method
def put_images(context):
    Commons.add_Images(context)

# call the create schedule method
def create_schedule(context):
    Commons.create_Schedule(context)

# call the send to sequence method
def send_sequence(context):
    Commons.send_to_Sequence(context)