import requests
from requests.auth import HTTPDigestAuth
from xlrd import *
from pandas import *
import random
import string
from time import gmtime,strftime
# from steps.Content import proxies,http_request_headers
from dateutil.parser import parse
from dateutil.relativedelta import *
from datetime import *
import json

# ---------------------------------------------------------------------------
# This file contain various helper methods required to achieve concrete tasks
# ---------------------------------------------------------------------------

# dictionary to hold the platform urls
platform_urls = {
        "Sky OTT": "https://preproduction-movida.bebanjo.net/api/platforms/5159",
        "Sky STB HD": "https://preproduction-movida.bebanjo.net/api/platforms/5257",
        "Sky STB SD": "https://preproduction-movida.bebanjo.net/api/platforms/5160"
    }

# function to read the required values from the excel sheet
# use Pandas library to handle data
def  xlRead(context):
    print('Reading excel values...')
    df = pandas.read_excel('C:/Users/611576932/Documents/Parth Kinage/VOD2SKY/All_Contents.xlsx',sheet_name='Worksheet')

    context.Title_Name = df['Title:Name'][0]
    context.Title_Type = df['Title:Type'][0]
    context.Title_Tags = df['Title:Subgenre'][0]
    context.licensor = df['Licensor'][0]
    context.platform = df['Platform'][0]
    context.external_id = df['External Id'][0]
    context.put_up = df['Put Up'][0]
    context.take_down = df['Take Down'][0]
    context.TypeMeta = df['Type'][0]
    context.short_desc = df['Title:Short Description'][0]
    context.medium_desc = df['Title:Medium Description'][0]
    context.long_desc = df['Title:Long Description'][0]
    context.category = df['Title:Category'][0]
    context.subgenre = df['Title:Subgenre1'][0]
    context.release_year = df['Title:Release Year'][0]
    context.production_company_location = df['Title:Production Company Location'][0]
    context.linear_channel = df['BT Sport Digital:Linear Channel'][0]
    context.sky_placements = df['BT Sport Digital:Sky Placements'][0]
    context.rating = df['Editorial Version:Rating'][0]

# function to read required values from the json schema
def read_json(json_object):
    with open('C:/Users/611576932/PycharmProjects/test-e2e/features/input_json_schema/create_asset_data.json') as json_data:
        content_dict=json.load(json_data)

    if json_object=='title':
        return content_dict['title']
    elif json_object=='title_metadata':
        return content_dict['title_metadata']
    elif json_object=='images':
        return content_dict['images']
    elif json_object=='asset':
        return content_dict['asset']
    elif json_object=='asset_metadata':
        return content_dict['asset_metadata']
    elif json_object=='scheduling':
        return content_dict['scheduling']

# Accessors to fetch the required values
def get_subgenre(context):
    return context.subgenre

def get_release_year(context):
    return context.release_year

def get_production_company_location(context):
    return context.production_company_location

def get_linear_channel(context):
    return context.linear_channel

def get_sky_placements(context):
    return context.sky_placements

def get_Type_metadata(context):
    return context.TypeMeta

def get_short_desc(context):
    return context.short_desc

def get_medium_desc(context):
    return context.medium_desc

def get_long_desc(context):
    return context.long_desc

def get_category(context):
    return context.category

def get_Title_Name(context):
    return context.Title_Name

def get_Title_Type(context):
    return context.Title_Type

def get_Title_Tags(context):
    return context.Title_Tags

def get_external_id(context):
    randomId = ''.join([random.choice(string.ascii_letters+string.digits) for n in range(8)])
    return randomId

def get_licensor(context):
    return context.licensor

def get_platform(context):
    return context.platform

def get_put_up_time(context):
    return context.put_up

def get_take_down_time(context):
    return context.take_down

def get_rating(context):
    return context.rating

# dictionary to hold the title attributes
def valid_title_attributes(context):

    title_attr = {
        "Title_Name": get_Title_Name(context),
        "Title_Type": get_Title_Type(context),
        "Title_Tag": get_Title_Tags(context),
        "external_id": get_external_id(context),
        "licensor" : get_licensor(context),
        "platform" : get_platform(context),
        "put-up-time" : get_put_up_time(context),
        "take-down-time" : get_take_down_time(context),
        "metadata_type" : get_Type_metadata(context),
        "short_description" : get_short_desc(context),
        "medium_description" : get_medium_desc(context),
        "long_description" : get_long_desc(context),
        "category" : get_category(context),
        "subgenre" : get_subgenre(context),
        "release_year" : get_release_year(context),
        "production_company_location" : get_production_company_location(context),
        "linear_channel" : get_linear_channel(context),
        "sky_placements" : get_sky_placements(context),
        "rating" : get_rating(context)
    }
    return  title_attr

# generate the payload required to update the metadata of an asset
def get_Asset_Metadata_payload(rating):
    return '''<?xml version="1.0" encoding="UTF-8"?>
    <metadata type="document">
        <rating>{}</rating>
    </metadata>
    '''.format(rating)

# generate the payload required to create a title
def get_Title_payload(name,licensor,title_type,tags,external_id):
    return '''<?xml version="1.0" encoding="UTF-8"?>
    <title>
        <name>{}</name>
        <link rel="licensor" href="{}"/>
        <title-type>{}</title-type>
        <tags>{}</tags>
        <external-id>{}</external-id>
    </title>
'''.format(name,licensor,title_type,tags,external_id)

# generate the payload for updating the metadata of the title
def get_Title_Metadata_payload(type,name,short,medium,long,category,subgenre,release_year,prod_location,first_linear,linear_channel,sky):
    return '''<?xml version="1.0" encoding="UTF-8"?>
    <metadata type="document">
        <type>{}</type>
        <name>{}</name>
        <short-description>{}</short-description>
        <medium-description>{}</medium-description>
        <long-description>{}</long-description>
        <category>{}</category>
        <subgenres type="array">
            <subgenre>{}</subgenre>
        </subgenres>
        <release-year>{}</release-year>
        <production-company-locations type="array">
             <production-company-location>{}</production-company-location>
        </production-company-locations>
        <first-shown-on-linear>{}</first-shown-on-linear>
        <linear-channel>{}</linear-channel>
        <sky-placements type="array">
            <sky-placement>{}</sky-placement>
        </sky-placements>
    </metadata>
    '''.format(type,name,short,medium,long,category,subgenre,release_year,prod_location,first_linear,linear_channel,sky)

# generate the payload to create a schedule for the title
def get_schedule_payload(put_up_time,take_down_time,external_id,platform_url):
    return '''<?xml version="1.0" encoding="UTF-8"?>
    <scheduling>
       <put-up>{}</put-up>
       <take-down>{}</take-down>
       <external-id>{}</external-id>
       <link href="{}" rel="platform"></link>
</scheduling>
    '''.format(put_up_time,take_down_time,external_id,platform_url)

# generate the payload to send a title to the sequence API
def get_workflow_payload():
    return'''<?xml version="1.0" encoding="UTF-8"?>
    <workflow></workflow>'''

# generate a paylod to create an asset
def get_Asset_payload(name,desc):
    return '''<?xml version="1.0" encoding="UTF-8"?>
    <asset>
        <name>{}</name>
        <description>{}</description>
    </asset>
    '''.format(name,desc)

# generate a payload to upload the images on the platform for a title
def get_Images_payload(file_url):
    return '''<?xml version="1.0" encoding="UTF-8"?>
      <image>
        <encoding>jpg</encoding>
        <file-url>{}</file-url>
      </image>
        '''.format(file_url)

# returns the endpoint of the titles api
def get_Title_URL(context):
    return context.basic_application_url+'/titles'

# get the licensor url
def get_licensor_URL(context):
    title_attributes = valid_title_attributes(context)
    name = title_attributes['licensor']
    return context.basic_application_url+'/licensors?name='+name

# get the api endpoints of title and licensor and store it in a dictionary
def get_API_endpoints(context):

    API_endpoints = {
        "Title": get_Title_URL(context),
        "licensor": get_licensor_URL(context)
    }

    return API_endpoints

# return the authentication headers
def get_auth_headers(context):
    auth = HTTPDigestAuth('robot_bttv_autocreate', '0a90c239a')
    return auth

# verify the response code of a request
# pass the desired response code as a parameter
def check_status_code(context, code):
    assert context.response['status_code'] == code

# get the start time for a schedule
def get_start_time():
    actualDayNum = strftime("%d", gmtime())
    MonNum = int(strftime("%m", gmtime()))
    dayNum = int(actualDayNum)
    if dayNum==1:
        reducedayNum = str(30)
        reduceMonthNum = str(MonNum-1)
        if len(reduceMonthNum) == 1:
            reduceMonthNo = "0{0}".format(str(MonNum - 1))
            return strftime("%Y-"  +reduceMonthNo +"-"+ reducedayNum + "T%H:%M:%SZ", gmtime())
        else:
            return strftime("%Y-"  +reduceMonthNum +"-"+ reducedayNum + "T%H:%M:%SZ", gmtime())
    else:
        reducedayNum = str(dayNum - 1)
    if len(reducedayNum) == 1:
        reducedayNo = "0{0}".format(str(dayNum - 1))
        return strftime("%Y-%m-" + reducedayNo + "T%H:%M:%SZ", gmtime())
    else:
        return strftime("%Y-%m-" + reducedayNum + "T%H:%M:%SZ", gmtime())
    # Working towards efficient time management
    # dt = date.today() - timedelta(30)
    # print(dt)
    # strdate = str(dt)
    # year = strdate[:4]
    # print("Year " + strdate[:4])
    # month = strdate[5:-3]
    # print("Month " + strdate[5:-3])
    # day = strdate[8:]
    # print("Day " + strdate[8:])
    # # start = f"{day}/{month}/{year} 18:55"
    # # print("Formatted date - "+start)
    # statd_date = "{}/{}/{} 09:00".format(day, month, year)
    # print(statd_date)

# get the end time for a schedule
def get_end_time():
    actualDayNum = strftime("%d", gmtime())
    dayNum = int(actualDayNum)
    adddayNum = str(dayNum + 1)
    if len(adddayNum) == 1:
        adddayNo = "0{0}".format(str(dayNum + 1))
        return strftime("%Y-%m-" + adddayNo + "T%H:%M:%SZ", gmtime())
    else:
        return strftime("%Y-%m-" + adddayNum + "T%H:%M:%SZ", gmtime())

# get the first shown on linear time for a title
# the schedule time needs to be manipulated to get this time
def get_first_shown_on_linear(context):
    title_attributes = valid_title_attributes(context)
    start_time = str(title_attributes['put-up-time'])
    prev_dt = parse(start_time)
    start_hour = prev_dt.strftime("%H00")
    # use the relative delta module from the dateutil library to manipulate time
    delta=relativedelta(days=-1,hours=+2)
    new_dt = prev_dt+delta
    first_shown_on_linear = new_dt.strftime("%d/%m/%Y ("+start_hour+"-%H00)")
    return first_shown_on_linear




