from behave import *
from features.steps import Content

# ----------------------------------------------------
# call the respective functions according to the flow
# ----------------------------------------------------

# this function is mapped to the given statement of the text mentioned in the parameter
# this text is present in the features file as a given hence the annotation @given
@given('Valid Catchup schedule entry in Movida')
def create_schedule_entry(context):
    Content.set_base_url(context,"https://preproduction-movida.bebanjo.net/api")
    Content.set_headers(context)
    Content.create_update_title(context)
    Content.put_images(context)
    Content.add_asset_metadata(context)
    Content.create_schedule(context)
    Content.send_sequence(context)


